package com.gitlab.rgander.blog.aspectjltw.example;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LombokedObject {

	private String title;
	
}
