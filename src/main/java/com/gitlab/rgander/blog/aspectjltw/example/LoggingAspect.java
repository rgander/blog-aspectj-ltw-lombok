package com.gitlab.rgander.blog.aspectjltw.example;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
public class LoggingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

    @Pointcut("execution(* com.gitlab.rgander.blog.aspectjltw.example.*.*(..))")
    public void serviceExecution() {}
    
    @Around("serviceExecution()")
    public Object aroundExecution(final ProceedingJoinPoint joinPoint) throws Throwable {
        LOGGER.info("Logging around aspect: {}", joinPoint);
        return joinPoint.proceed();
    }

}
