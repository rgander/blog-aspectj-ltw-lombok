package com.gitlab.rgander.blog.aspectjltw.example;

import org.springframework.stereotype.Service;

@Service
public class ExampleService {

	public String doBusinessLogic() {
		LombokedObject lombokedObject = new LombokedObject("test");
	
		return lombokedObject.getTitle();
	}
	
}
