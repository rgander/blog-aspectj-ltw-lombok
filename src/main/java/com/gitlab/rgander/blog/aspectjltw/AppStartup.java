package com.gitlab.rgander.blog.aspectjltw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.gitlab.rgander.blog.aspectjltw.example.ExampleService;

@Component
public class AppStartup implements ApplicationListener<ApplicationStartedEvent> {

	@Autowired
	private ExampleService exampleService;
	
	public void onApplicationEvent(ApplicationStartedEvent event) {
		this.exampleService.doBusinessLogic();
	}

}
