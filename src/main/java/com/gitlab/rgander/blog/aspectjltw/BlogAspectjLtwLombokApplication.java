package com.gitlab.rgander.blog.aspectjltw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableLoadTimeWeaving;

@SpringBootApplication
@EnableLoadTimeWeaving
public class BlogAspectjLtwLombokApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogAspectjLtwLombokApplication.class, args);
	}

}
